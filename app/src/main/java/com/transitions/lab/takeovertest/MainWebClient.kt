package com.transitions.lab.takeovertest

import android.os.Build
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.transitions.lab.takeovertest.WebFragment.OnWebFragmentInteractionListener

class MainWebClient(private val mListener: OnWebFragmentInteractionListener) : WebViewClient() {

    companion object {
        private const val SELECT_TICKETS_URL = "adbinapp://confirm/?url=http://disneyworld.disney.go.com/events/mickeys-very-merry-christmas-party/purchase/?CMP=ILC-WDWFY17Q4FY17Q1MVMCPApp0001"
        private const val CANCEL_URL = "adbinapp://cancel"
    }

    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        if (request == null) return true

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            when (request.url.toString()) {
                SELECT_TICKETS_URL -> mListener.onSelectTicketsClicked()
                CANCEL_URL -> mListener.onCancelClicked()
                else -> return true
            }
        }


        return true
    }
}