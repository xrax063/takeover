package com.transitions.lab.takeovertest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), WebFragment.OnWebFragmentInteractionListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btWebView.setOnClickListener {
            flMainContainer.visibility = View.VISIBLE
            supportFragmentManager.beginTransaction()
                    .replace(R.id.flMainContainer, WebFragment.newInstance("file:///android_asset/takeoverExample.html"))
                    .addToBackStack(WebFragment.TAG)
                    .commit()
        }
    }

    override fun onSelectTicketsClicked() {
        Toast.makeText(this, "Select ticket", Toast.LENGTH_SHORT).show()
    }

    override fun onCancelClicked() {
        Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show()
    }
}
