package com.transitions.lab.takeovertest

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView

private const val ARG_URL = "url"

class WebFragment : Fragment() {

    interface OnWebFragmentInteractionListener {
        fun onSelectTicketsClicked()
        fun onCancelClicked()
    }

    companion object {
        val TAG: String = WebFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(url: String) =
                WebFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_URL, url)
                    }
                }
    }

    private var mUrl: String? = null
    private var mListenerWeb: OnWebFragmentInteractionListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnWebFragmentInteractionListener) {
            mListenerWeb = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnWebFragmentInteractionListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mUrl = it.getString(ARG_URL)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_web, container, false)

        view.findViewById<WebView>(R.id.webView).apply {
            loadUrl("file:///android_asset/takeoverExample.html")
            if (mListenerWeb != null) webViewClient = MainWebClient(mListenerWeb!!)
        }

        return view
    }

    override fun onDetach() {
        super.onDetach()
        mListenerWeb = null
    }
}
